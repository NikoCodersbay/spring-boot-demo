package com.example.Login.controller;

import com.example.Login.dto.RegisterDTO;
import com.example.Login.dto.UserDTO;
import com.example.Login.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<UserDTO> registerUser(@RequestBody RegisterDTO registerDTO){

        UserDTO userDTO = userService.registerUser(registerDTO);
        return new ResponseEntity<>(userDTO, HttpStatus.CREATED);
    }

    @GetMapping("all")
    public ResponseEntity<Set<UserDTO>> getAll(){
        //Kurz schreibform für die ResponesEntity
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }

    @GetMapping("{userId}")
    public ResponseEntity<UserDTO> get(@PathVariable int userId) {
        return new ResponseEntity<>(userService.getById(userId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<UserDTO> get(@RequestBody String userName) {
        return new ResponseEntity<>(userService.getByName(userName), HttpStatus.OK);
    }
}

