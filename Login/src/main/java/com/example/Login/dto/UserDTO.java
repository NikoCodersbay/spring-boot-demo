package com.example.Login.dto;

import com.example.Login.enums.UserRole;
import jakarta.persistence.Column;

public class UserDTO {

    private int userId;

    private String userName;

    private String eMail;

    private String birthDate;

    private UserRole userRole;

    public UserDTO() {
    }

    public UserDTO(int userId, String userName, String eMail, String birthDate, UserRole userRole) {
        this.userId = userId;
        this.userName = userName;
        this.eMail = eMail;
        this.birthDate = birthDate;
        this.userRole = userRole;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
