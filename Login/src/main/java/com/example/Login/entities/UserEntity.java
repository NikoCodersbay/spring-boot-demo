package com.example.Login.entities;

import com.example.Login.enums.UserRole;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;

    private String userName;

    @Column(name = "password")
    private String userPasword;

    private String eMail;

    private String birthDate;

    private UserRole userRole;


   @OneToMany(mappedBy = "userEntity")
    private List<AddressEntity> addressEntityList = new ArrayList<>();

    public UserEntity() {
    }

    public UserEntity(String userName, String userPasword, String eMail, UserRole userRole) {
        this.userName = userName;
        this.userPasword = userPasword;
        this.eMail = eMail;
        this.userRole = userRole;
    }

    public UserEntity(String userName, String userPasword, String eMail, String birthDate, UserRole userRole) {
        this.userName = userName;
        this.userPasword = userPasword;
        this.eMail = eMail;
        this.birthDate = birthDate;
        this.userRole = userRole;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPasword() {
        return userPasword;
    }

    public void setUserPasword(String userPasword) {
        this.userPasword = userPasword;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public List<AddressEntity> getAddressEntityList() {
        return addressEntityList;
    }

    public void setAddressEntityList(List<AddressEntity> addressEntityList) {
        this.addressEntityList = addressEntityList;
    }
}
