package com.example.Login.enums;

public enum UserRole {
    ADMIN("Administrator"),
    MAINTAINER("Betreuer"),
    USER("User");

    private String label;

    private UserRole(String label){
        this.label = label;
    }
}
