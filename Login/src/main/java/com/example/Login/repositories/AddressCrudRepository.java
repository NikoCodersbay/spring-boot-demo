package com.example.Login.repositories;

import com.example.Login.entities.AddressEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressCrudRepository extends CrudRepository<AddressEntity, Integer> {
}
