package com.example.Login.repositories;

import com.example.Login.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.management.Query;

@Repository
public interface UserCrudRepository extends CrudRepository<UserEntity, Integer> {
    UserEntity findByUserName(String userName);
}
