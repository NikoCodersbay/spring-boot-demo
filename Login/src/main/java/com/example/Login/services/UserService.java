package com.example.Login.services;

import com.example.Login.dto.RegisterDTO;
import com.example.Login.dto.UserDTO;
import com.example.Login.entities.AddressEntity;
import com.example.Login.entities.UserEntity;
import com.example.Login.repositories.AddressCrudRepository;
import com.example.Login.repositories.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    UserCrudRepository userCrudRepository;

    @Autowired
    AddressCrudRepository addressCrudRepository;

    public UserDTO registerUser(RegisterDTO registerDTO){
//        if(!validateEmail(registerDTO.geteMail())){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
//        }


        UserEntity userEntity = new UserEntity(registerDTO.getUserName(),
                registerDTO.getUserPassword(),
                registerDTO.geteMail(),
                registerDTO.getUserRole());

        AddressEntity addressEntity = new AddressEntity(registerDTO.getStreet(), registerDTO.getHouseNumber(), registerDTO.getStairway(), registerDTO.getDoorNumber(), registerDTO.getPostCode(), registerDTO.getCity(), userEntity);

        AddressEntity addressEntity2 = new AddressEntity("asdf", "asda", "asdas", "asdasd", "asdad", "asdasd", userEntity);

        userEntity.getAddressEntityList().add(addressEntity);

        userEntity.getAddressEntityList().add(addressEntity2);

        userCrudRepository.save(userEntity);

        addressCrudRepository.save(addressEntity);

        addressCrudRepository.save(addressEntity2);









        userCrudRepository.save(userEntity);

        //userEntitiy wird in eine UserDTO umgewandelt
        return new UserDTO(userEntity.getUserId(),
                            userEntity.getUserName(),
                            userEntity.geteMail(),
                            userEntity.getBirthDate(),
                            userEntity.getUserRole());

    }

//    public boolean validateEmail(String eMail){
//        Pattern pattern = Pattern.compile("^(. +)@(\\S+) $", Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(eMail);
//        return matcher.find();
//    }

    public Set<UserDTO> getAll(){
        //FindAll gibt ein Iterable zurück
        Iterable<UserEntity> userEntityIterable = userCrudRepository.findAll();

        Set<UserDTO> userDTOSet = new HashSet<>();

        //Für jede gefundene UserEntity aus der Datenbank wird ein neues USerDTO erstellt und in einem Set gespeichert, dass dann ans Frontend geschickt wird
        for (UserEntity userEntity: userEntityIterable) {
            UserDTO userDTO = new UserDTO(userEntity.getUserId(),
                    userEntity.getUserName(),
                    userEntity.geteMail(),
                    userEntity.getBirthDate(),
                    userEntity.getUserRole());

            userDTOSet.add(userDTO);
        }

        return userDTOSet;
    }

    public UserDTO getById(int userId) {
        Optional<UserEntity> userEntityOptional = userCrudRepository.findById(userId);
        if(userEntityOptional.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        UserEntity userEntity = userEntityOptional.get();

        return new UserDTO(userEntity.getUserId(), userEntity.getUserName(), userEntity.geteMail(), userEntity.getBirthDate(), userEntity.getUserRole());

    }

    public UserDTO getByName(String userName) {
       UserEntity userEntity = userCrudRepository.findByUserName(userName);

        return new UserDTO(userEntity.getUserId(), userEntity.getUserName(), userEntity.geteMail(), userEntity.getBirthDate(), userEntity.getUserRole());

    }
}
